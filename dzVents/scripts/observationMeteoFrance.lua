--[[

--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------

--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'météo France observation'
local scriptVersion     = '1.0'
local response          = "meteoFranceobs_response"
return {
    active = false,
    on =        {       timer           =   { "every 1 minutes" },
                        httpResponses   =   {  response } },

    logging =   {   -- level    =   domoticz.LOG_DEBUG,
                    -- level    =   domoticz.LOG_INFO,             -- Seulement un niveau peut être actif; commenter les autres
                    level    =   domoticz.LOG_ERROR,            -- Only one level can be active; comment others
                    -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                    marker  =   scriptName..' v'..scriptVersion },

    execute = function(dz, item)
        local function logWrite(str,level)
            dz.log(tostring(str),level or dz.LOG_DEBUG)
        end
		
	local function date2string(entier)
	    return os.date("%c", entier)
	end


        if item.isHTTPResponse then
	    local json = dz.utils.fromJSON(item.data)
	    local position = json["position"]
	    local mise_a_jour = json["updated_on"]
	    local observation = json["observation"]


            local temp = tonumber(observation["T"])
            local speed = tonumber(observation["wind"]["speed"])
            local direction = tonumber(observation["wind"]["direction"])
            local icon = observation["wind"]["icon"]
           
	    logWrite(temp)
	    logWrite(speed)
	    logWrite(direction)
	    logWrite(icon)
	    logWrite("Date prévision : "..date2string(mise_a_jour))
        else
            local url = 'http://webservice.meteofrance.com/observation?token=__Wj7dVSTjV9YGu1guveLyDq0g7S7TfTjaHBTPTpO0kj8__&lat=49.12&lon=2.52&lang=fr&id='
            dz.openURL({
                  url = url,
                        method = "GET",
                        callback = response})
        end
    end
}
