--[[
alerteMeteoFrancePluie.lua
author/auteur = papoo
update/mise à jour = 11/05/2019
création = 09/03/2019
https://pon.fr/dzvents-alerte-previsions-de-pluie-prochaines-60-minutes
https://github.com/papo-o/domoticz_scripts/blob/master/dzVents/scripts/alerteMeteoFrancePluie.lua
https://easydomoticz.com/forum/viewtopic.php?f=17&t=5492

Principe : récupérer via l'API non documentée de météo France 
les informations de précipitation de votre commune sur un device alert et/ou text
la carte des départements couverts par ce service (ils ne le sont pas tous)
http://www.meteofrance.com/previsions-meteo-france/previsions-pluie

--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local dVent             = "Météo France"

--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'Météo France vent'
local scriptVersion     = '1.0'
local response          = "meteoFrance_vent"
return {
    active = true,
    on =        {       timer           =   { "every 10 minutes" },
                        httpResponses   =   {  response } },

    logging =   {   -- level    =   domoticz.LOG_DEBUG,
                    -- level    =   domoticz.LOG_INFO,             -- Seulement un niveau peut être actif; commenter les autres
                    level    =   domoticz.LOG_ERROR,            -- Only one level can be active; comment others
                    -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                    marker  =   scriptName..' v'..scriptVersion },

    execute = function(dz, item)

        local deviceVent = dz.devices(dVent)

        local function logWrite(str,level)
            dz.log(tostring(str),level or dz.LOG_DEBUG)
        end
        local function seuilAlerte(level)
            if level == 0 or level == nil then return dz.ALERTLEVEL_GREY end
            if level == 1 then return dz.ALERTLEVEL_GREEN end
            if level == 2 then return dz.ALERTLEVEL_YELLOW end
            if level == 3 then return dz.ALERTLEVEL_ORANGE end
            if level == 4 then return dz.ALERTLEVEL_RED end
        end
	local function quadrants(degrees)
	    local quadrants = {"NNE","NE","ENE","E","ESE","SE","SSE","S","SSW","SW","WSW","W","WNW","NW","NNW"}
			                
	    if degrees > 348.75 or degrees <= 11.25 then
                return "N"
            else 
                local index = tonumber(math.floor((degrees - 11.25) / 22.5)+1)
                return quadrants[index]
            end
        end
	local function date2string(entier)	
	    return os.date("%c", entier)
	end
	local function date2stringheure(entier)	
	    return tonumber(os.date("%H", entier))..":"..(os.date("%M", entier))..""
        end 

        if item.isHTTPResponse then
	    local json = dz.utils.fromJSON(item.data)
	    local position = json["position"]
	    local mise_a_jour = json["updated_on"]
	    local observation = json["observation"]
	    logWrite("Date prévision : "..date2stringheure(mise_a_jour))

            local windGust = dz.variables('meteoForecastWindGust').value
	    local windChill = dz.variables('meteoForecastWindChill').value 
	    local temperature = tonumber(observation["T"])
	    local windSpeed = tonumber(observation["wind"]["speed"])
	    local windBearing = tonumber(observation["wind"]["direction"])
	    local windIcon = tonumber(observation["wind"]["icon"])
	    local direction = quadrants(windBearing)

	    deviceVent.updateWind(windBearing,tostring(direction),windSpeed,windGust,temperature,windChill)
        else
            local url = 'http://webservice.meteofrance.com/observation?token=__Wj7dVSTjV9YGu1guveLyDq0g7S7TfTjaHBTPTpO0kj8__&lat=49.12&lon=2.52&lang=fr&id='
            dz.openURL({
                       url = url,
                       method = "GET",
                       callback = response})
        end
    end
}
