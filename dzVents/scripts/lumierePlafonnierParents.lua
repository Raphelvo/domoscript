--[[ lumierePlafonnierParents.lua for [ domoticzVents >= 2.4 ]

author/auteur = raphelvo
creation = 19/05/2021
--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local deviceActivate = 'Plafonnier Parents' -- nom ou idx du device de declenchement 
local deviceTarget = 'Ampoule LIDL' -- Device to activate

--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'Script plafonnier Parents'
local scriptVersion     = '1.0'

return { 
    active  = true,
    on      =   {   
                    devices  =  { deviceActivate }
                },

    logging =   {
                   level    =   domoticz.LOG_DEBUG,       -- Seulement un niveau peut être actif; commenter les autres
                   -- level    =   domoticz.LOG_ERROR,
                   marker =  scriptName..' v'..scriptVersion },
	        
    execute = function(domoticz, item)
    
        local function logWrite(str,level)         -- Support function for shorthand debug log statements
            domoticz.log(tostring(str),level or domoticz.LOG_DEBUG)
        end
        
	-- Verifie l'état de la lumière
	if (item.active) then
            -- logWrite("Plafonnier ON", domoticz.LOG_ERROR)
            domoticz.devices(deviceTarget).switchOn()
        else
            -- logWrite("Plafonnier OFF", domoticz.LOG_ERROR)
            domoticz.devices(deviceTarget).switchOff()
	end 
        --domoticz.devices(deviceActivate).switchOff().checkFirst()
    end
} 
