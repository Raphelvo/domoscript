#! /bin/bash
# lancé par cron table pour pas bloquer Domoticz

# Login / IDX 16 dans domoticz
LOGIN=`curl "http://127.0.0.1:9090/json.htm?type=command&param=getuservariable&idx=16" 2> /dev/null | grep Value | cut -d'"' -f4`
# Pass / IDX 17 
PASS=`curl "http://127.0.0.1:9090/json.htm?type=command&param=getuservariable&idx=17" 2> /dev/null | grep Value | cut -d'"' -f4`
# Command
pushd /home/pi/domoticz/scripts/dzVents/scripts > /dev/null
pronote-fetch https://0601766u.index-education.net/pronote/ $LOGIN $PASS hdf parent > /dev/null
chown pi:pi result.json > /dev/null
popd > /dev/null

