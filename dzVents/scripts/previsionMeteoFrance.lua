--[[
alerteMeteoFrancePluie.lua
author/auteur = papoo
update/mise à jour = 11/05/2019
création = 09/03/2019
https://pon.fr/dzvents-alerte-previsions-de-pluie-prochaines-60-minutes
https://github.com/papo-o/domoticz_scripts/blob/master/dzVents/scripts/alerteMeteoFrancePluie.lua
https://easydomoticz.com/forum/viewtopic.php?f=17&t=5492

Principe : récupérer via l'API non documentée de météo France 
les informations de précipitation de votre commune sur un device alert et/ou text
la carte des départements couverts par ce service (ils ne le sont pas tous)
http://www.meteofrance.com/previsions-meteo-france/previsions-pluie

--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local UV                = "Indice U.V."

--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'Météo France prévision'
local scriptVersion     = '1.0'
local response          = "meteoFrance_forecast"
return {
    active = true,
    on =        {       timer           =   { "every 15 minutes" },
                        httpResponses   =   {  response } },

    logging =   {   --level    =   domoticz.LOG_DEBUG,
                    --level    =   domoticz.LOG_INFO,             -- Seulement un niveau peut être actif; commenter les autres
                    level    =   domoticz.LOG_ERROR,            -- Only one level can be active; comment others
                    -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                    marker  =   scriptName..' v'..scriptVersion },

    execute = function(dz, item)

        local deviceUV        = dz.devices(UV)
	local jours = { [0] = "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" } 
	local demains = { [0] = "Hier", "Aujourd'hui", "Demain", "Après-demain", "Après aprés-demain" }
	local mois = { [0] = "vide", "janvier", "février", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "octobre", "novembre", "decembre" }

        local function logWrite(str,level)
            dz.log(tostring(str),level or dz.LOG_DEBUG)
        end
        local function seuilAlerte(level)
            if level == 0 or level == nil then return dz.ALERTLEVEL_GREY end
            if level == 1 then return dz.ALERTLEVEL_GREEN end
            if level == 2 then return dz.ALERTLEVEL_YELLOW end
            if level == 3 then return dz.ALERTLEVEL_ORANGE end
            if level == 4 then return dz.ALERTLEVEL_RED end
        end
	local function date2string(entier)	
	    return os.date("%c", entier)
	end
	local function date2stringheure(entier)	
	    return tonumber(os.date("%H", entier))..":"..(os.date("%M", entier))..""
        end 

        if item.isHTTPResponse then
	    local json = dz.utils.fromJSON(item.data)
	    local position = json["position"]
	    local mise_a_jour = json["updated_on"]
	    local daily_forecast = json["daily_forecast"]
	    local forecast = json["forecast"]
	    local probability_forecast = json["probability_forecast"]
	    logWrite("Date prévision : "..date2stringheure(mise_a_jour))
	    for i = 1, 3, 1  -- Boucle dans les 9 niveaux de prévisions
            do
		local dt = daily_forecast[i]["dt"]
		local t = daily_forecast[i]["T"]
	        local uv = "    Indice UV: "..daily_forecast[i]["uv"].."\n"
		local temperature = "    Température min/max: "..t["min"].." °C/"..t["max"].." °C\n"
		local precipitation = "    Précipitation: "..daily_forecast[i]["precipitation"]["24h"].." ml"
		local meteo = daily_forecast[i]["weather12H"]["desc"].."\n"
		local nbj = tonumber(os.date("%w", dt))
		local jourdumois = tonumber(os.date("%d", dt))
		local nbm = tonumber(os.date("%m", dt))
		alertText = jours[nbj].." "..jourdumois.." "..mois[nbm]..": "
		alertText = alertText..meteo..temperature..precipitation
		logWrite(alertText)
                local devicePrevision = dz.devices(demains[i])
	        devicePrevision.updateAlertSensor(seuilAlerte(1), alertText)
            end

	    --ther12H 	 Indice UV du jour est le 1er element de la table daily_forecast
	    deviceUV.updateUV(tonumber(daily_forecast[1]["uv"]))
	    dz.variables('meteoUV').set(tonumber(daily_forecast[1]["uv"]))
	    dz.variables('meteoForecastWindChill').set(forecast[1]["T"]["windchill"])
	    dz.variables('meteoForecastWindGust').set(forecast[1]["wind"]["gust"])
        else
            local url = 'http://webservice.meteofrance.com/forecast?token=__Wj7dVSTjV9YGu1guveLyDq0g7S7TfTjaHBTPTpO0kj8__&lat=49.12&lon=2.52&lang=fr&id='
            dz.openURL({
                  url = url,
                        method = "GET",
                        callback = response})
        end
    end
}
