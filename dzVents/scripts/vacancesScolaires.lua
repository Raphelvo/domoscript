--[[
/home/pi/domoticz/scripts/dzVents/scripts/vacancesScolaires.lua
author/auteur = papoo
update/mise à jour = 18/07/2020
création = 05/08/2017
https://pon.fr/dzvents-vacances-scolaires-par-zone-et-academie/
https://github.com/papo-o/dz_scripts/blob/master/dzVents/scripts/vacancesScolaires.lua
https://easydomoticz.com/forum/viewtopic.php?f=17&t=9126

V1.xx  : https://github.com/papo-o/domoticz_scripts/blob/master/Lua/script_time_vacances_scolaires.lua

Principe : récupérer via l'API  de data.education.gouv (https://data.education.gouv.fr/explore/dataset/fr-en-calendrier-scolaire/api/?disjunctive.description)
les informations de vacances scolaires pour une date, une zone et une academie

--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------

local zone                  = 'B'                    -- Indiquer ici la zone (A, B ou C)
local location              = 'Amiens'               -- Indiquer ici l'academie (https://fr.wikipedia.org/wiki/Acad%C3%A9mie_(%C3%A9ducation_en_France))
local holidayNow            = 'Vacances Scolaires'   -- Indiquer ici le nom du device vacances aujourd'hui de type switch nil si inutilisé
local holidayTomorrow       = nil                    -- Indiquer ici le nom du device vacances demain de type switch nil si inutilisé
local dayOff                = 'Jour chomé'           -- Indiquer ici le nom du device pour les jours chomé 

--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------

local scriptName            = 'Vacances Scolaires'
local scriptVersion         = '2.03'
local response              = 'dataEducation_response'

return {
    active = true,
    on =        {       timer           =   { "at 0:05" },
                        httpResponses   =   {  response },
			devices         =   { holidayNow }
		},


    logging =   {   level    =   domoticz.LOG_DEBUG,
                    -- level    =   domoticz.LOG_INFO,             -- Seulement un niveau peut être actif; commenter les autres
                    -- level    =   domoticz.LOG_ERROR,            -- Only one level can be active; comment others
                    -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                    marker  =   scriptName..' v'..scriptVersion },

    execute = function(dz, item)
	--=================================================================================================--
	-- Variable locale 
	
        local _, round = dz.utils._, dz.utils.round
        local H0, H1, C0 = 0, 0, 0
        local function logWrite(str,level)
            dz.log(tostring(str),level or dz.LOG_DEBUG)
        end

	--=================================================================================================--
        local function date2timestamp(now)
            a, b, Y, M, D = string.find(now, "(%d+)-(%d+)-(%d+)")
            return os.time{year=Y, month=M, day=D, hour=00, minute=00}
        end

	--=================================================================================================--
        local function schoolYear(dayOfYear)
            if tonumber(dayOfYear) > 244 then
                schoolYear = dz.time.year .. "-" ..(tonumber(dz.time.year) + 1)
            else
                schoolYear = (tonumber(dz.time.year) - 1) .. "-" ..dz.time.year
            end
            return schoolYear
        end

	--=================================================================================================--
	-- -- annee : année (Integer) dont on désire connaître le jour de Pâques (ex : 2014)
	-- -- La fonction n'effectue le calcul que si l'année a changée depuis son dernier appel
	local function calculPaques(annee)
                local a = math.floor(annee/100)
	        local b = math.fmod(annee,100)
	        local c = math.floor((3*(a+25))/4)
	        local d = math.fmod((3*(a+25)),4)
	        local e = math.floor((8*(a+11))/25)
	        local f = math.fmod((5*a+b),19)
	        local g = math.fmod((19*f+c-e),30)
                local h = math.floor((f+11*g)/319)
                local j = math.floor((60*(5-d)+b)/4)
	        local k = math.fmod((60*(5-d)+b),4)
                local m = math.fmod((2*j-k-g+h),7)
                local n = math.floor((g-h+m+114)/31)
                local p = math.fmod((g-h+m+114),31)
                local jour = p + 1
                local mois = n
                return os.time{year=annee, month=mois, day=jour,hour=0 ,min=0}
	end

	--=================================================================================================--
	-- Procedure principale : Recherche de vacances scolaires
	--=================================================================================================--

        local Timestamp = dz.time.dDate
        --local Timestamp = date2timestamp("2019-12-20") -- pour test (uniquement sur l'année en cours)
        local dayOfYear = tonumber(os.date("%j"))
        --logWrite('vacances scolaires '.. schoolYear(dayOfYear))
	local yearCurrent = tonumber(os.date("%Y"))
	local dayOfWeek = tonumber(os.date("%w"))

        if (item.isHTTPResponse and item.trigger == response) then
            if (not item.isJSON) then
                logWrite('Last http response was not what expected. Trigger: '..item.trigger,dz.LOG_ERROR)
            else

		-- Calcul vacances scolaire
                local holidays   = {}
                local start_date = {}
                local end_date   = {}
                holidays = item.json.records

                if holidays ~= nil then
                    for i, Result in ipairs( holidays ) do
                        start_date[i] = Result.fields.start_date
                        end_date[i] = Result.fields.end_date

                        -- vacances aujourd'hui
                        if     (date2timestamp(start_date[i]) < Timestamp or date2timestamp(start_date[i]) == Timestamp) 
			    and (date2timestamp(end_date[i]) > Timestamp or date2timestamp(end_date[i]) == Timestamp) then
                            logWrite('date de début des vacances '.. tostring(start_date[i]))
                            logWrite('date de fin des vacances '.. tostring(end_date[i]))
                            H0 = 1
                        end
                        --vacances demain
                        if      (date2timestamp(start_date[i]) < (Timestamp + 24*3600) or date2timestamp(start_date[i]) == (Timestamp + 24*3600)) 
			    and (date2timestamp(end_date[i]) > (Timestamp + 24*3600) or date2timestamp(end_date[i]) == (Timestamp + 24*3600)) then
                            logWrite('date de début des vacances '.. tostring(start_date[i]))
                            logWrite('date de fin des vacances '.. tostring(end_date[i]))
                            H1 = 1
                        end
                    end
                    logWrite('H0 : '..tostring(H0))
                    if holidayNow ~= nil then
                        if H0 == 1 then
			    dz.globalData.dayHoliday = true
			    dz.variables('dayHoliday').set('true')
                            dz.devices(holidayNow).switchOn().checkFirst()
                            logWrite('Device '..holidayNow..' sur ON')
                        else
			    dz.globalData.dayHoliday = false 
			    dz.variables('dayHoliday').set('false')
                            dz.devices(holidayNow).switchOff().checkFirst()
                            logWrite('Device '..holidayNow..' sur OFF')
                        end
                    else
                        logWrite('pas de device pour les vacances du jour')
                    end

                    logWrite('H1 : '..tostring(H1))
                    if holidayTomorrow ~= nil then
                        if H1 == 1 then
			    dz.globalData.dayHolidayTomorrow = true
			    dz.variables('dayHolidayTomorrow').set('true')
                            -- PATCHEDOFF dz.devices(holidayTomorrow).switchOn().checkFirst()
                            logWrite('Device '..holidayTomorrow..' sur ON')
                        else
			    dz.globalData.dayHolidayTomorrow = false 
			    dz.variables('dayHolidayTomorrow').set('false')
                            -- PATCHEDOFF dz.devices(holidayTomorrow).switchOff().checkFirst()
                            logWrite('Device '..holidayTomorrow..' sur OFF')
                        end
                    else
                        logWrite('pas de device pour les vacances de demain')
                    end
                end

		-- Calcul jour chomé
		local jourFerieTab = {} -- Variable des jours fériés
		if ((dayOfWeek == 0) or (dayOfWeek == 6) or (H0 == 1)) then -- Weekend ou vacances
		    C0 = 1
		else
		    local today=os.date("%m-%d")
		    -- Dates fixes
		    jourFerieTab["01-01"] = true -- 1er janvier
	            jourFerieTab["05-01"] = true -- Fête du travail
	            jourFerieTab["05-08"] = true -- Victoire des alliés
	            jourFerieTab["07-14"] = true -- Fête nationale
	            jourFerieTab["08-15"] = true -- Assomption
	            jourFerieTab["11-01"] = true -- Toussaint
	            jourFerieTab["11-11"] = true -- Armistice
	            jourFerieTab["12-25"] = true -- Noël
	            -- Dates variables
		    local epochPaques = calculPaques(yearCurrent)
	            jourFerieTab[os.date("%m-%d",epochPaques)] = true             -- Pâques
	            jourFerieTab[os.date("%m-%d",epochPaques+24*60*60)] = true    -- Lundi de Pâques = Pâques + 1 jour
	            jourFerieTab[os.date("%m-%d",epochPaques+24*60*60*39)] = true -- Ascension = Pâques + 39 jours
	            jourFerieTab[os.date("%m-%d",epochPaques+24*60*60*49)] = true -- Pentecôte = Ascension + 49 jours

                    if (jourFerieTab[today]) then  -- (nldr : Both nil and false make a condition false)
	  	        C0 = 1
		    end
	        end
                if dayOff ~= nil then
                    if C0 == 1 then
			dz.globalData.dayOff = true 
			dz.variables('dayOff').set('true')
                        dz.devices(dayOff).switchOn().checkFirst()
                        logWrite('Device '..dayOff..' sur ON')
                    else
			dz.globalData.dayOff = false 
			dz.variables('dayOff').set('false')
                        dz.devices(dayOff).switchOff().checkFirst()
                        logWrite('Device '..dayOff..' sur OFF')
                    end
                else
                    logWrite('pas de device pour les jours chomés')
                end
            end
        else
            local schoolYear = schoolYear(dayOfYear)
            logWrite('vacances scolaires '.. schoolYear)
            local url = "https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-calendrier-scolaire&facet=start_date&facet=end_date&refine.zones=Zone+".. zone .."&rows=40&refine.annee_scolaire=".. schoolYear .."&refine.location=".. location.."&sort=-end_date&q=end_date%3E="..dz.time.rawDate
            dz.openURL({
                  url = url,
                        method = "GET",
                        callback = response})
        end

	--=========================================================================================--
    end
}
