--[[ modePhilou.lua for [ domoticzVents >= 2.4 ]

author/auteur = raphelvo
update/mise à jour = 20/10/2020
creation = 20/10/2020
--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local deviceActivate = 'Mode Philou' -- nom ou idx du device de declenchement 
local sceneToActivate = 'Baisser pour Philou'

--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'Mode Philou'
local scriptVersion     = '1.0'

return { 
    active  = true,
    on      =   {   timer = { '10 minutes after sunset' },
                    devices  =  { deviceActivate }
                },

    logging =   {
                   level    =   domoticz.LOG_DEBUG,       -- Seulement un niveau peut être actif; commenter les autres
                   -- level    =   domoticz.LOG_INFO,          
                   -- level    =   domoticz.LOG_ERROR,
                   -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                   marker =  scriptName..' v'..scriptVersion },
	        
    execute = function(domoticz, item)
    
        local function logWrite(str,level)         -- Support function for shorthand debug log statements
            domoticz.log(tostring(str),level or domoticz.LOG_DEBUG)
        end

        domoticz.scenes(sceneToActivate).switchOn()
        --domoticz.devices(deviceActivate).switchOff().checkFirst()
    end
} 
