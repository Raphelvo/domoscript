--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local device_Student_Average     = 'Note moyenne de Romain'
local device_Class_Average       = 'Note moyenne de la classe'
local device_Student_Todo_Tom    = 'Devoir pour demain'
local device_Student_Timetable   = 'Cours'
local device_Student_Todo_Resume = 'Devoirs'
local device_Automatismes        = 'Automatismes'

local var_student                = 'pronoteStudent1'
local var_token                  = 'pronoteToken'
local var_wakeUpdayOff           = 'child1WakeUpDayOff'        -- Heure de reveil quand pas de cours. Heure "HH:MM"
local var_wakeUpCalculated       = 'child1WakeUpCalculated'    -- Heure de reveil calculé d'apres la timetable. String format Time() 
--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName            = 'pronoteStudent1'
local scriptVersion         = '1.0'
local login_response        = "loginPronote_response"
local timetable_response    = "timetablePronote_response"
local homeworks_response    = "homeworksPronote_response"
local marks_response        = "marksPronote_response"
local logout_response       = "logoutPronote_response"

local temp_de_reveil        = 1*60*60+30*60     -- 1h30mn
local dico_matiere = {}

dico_matiere['PHYSIQUE-CHIMIE']      = 'Physique-Chimie'
dico_matiere['MATHEMATIQUES']        = 'Mathématiques  '
dico_matiere['TECHNOLOGIE']          = 'Technologie    '
dico_matiere['ANGLAIS LV1']          = 'Anglais        '
dico_matiere['ALLEMAND LV2']         = 'Allemand       '
dico_matiere['ED.PHYSIQUE & SPORT.'] = 'Sport          '
dico_matiere['EDUCATION MUSICALE']   = 'Musique        '
dico_matiere['FRANCAIS']             = 'Français       '
dico_matiere['HISTOIRE & GEOGRAPH.'] = 'Histoire Géo   '
dico_matiere['ARTS PLASTIQUES']      = 'Arts plastiques'
dico_matiere['SCIENCES VIE & TERRE'] = 'SVT            '
dico_matiere['08 - ARTS PLASTIQUES'] = '08 - ARTS'
dico_matiere['10 - MUSIQUE']         = '10 - MUSI'

return {
    active = true, 
    on =        {       timer           =   { "every 20 minutes" },
                        httpResponses   =   {  login_response, timetable_response, homeworks_response, marks_response, logout_response } },

    logging =   {   level    =   domoticz.LOG_DEBUG,
                    --level    =   domoticz.LOG_INFO,             -- Seulement un niveau peut être actif; commenter les autres
                    -- level    =   domoticz.LOG_ERROR,            -- Only one level can be active; comment others
                    -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                    marker  =   scriptName..' v'..scriptVersion },

    execute = function(dz, item)
        local Time = require('Time')

        -- Chargement de la conf par variable utilisateur
        local pronoteLogin = dz.variables('pronoteLogin').value
        local pronotePassword = dz.variables('pronotePassword').value
        local student1 = dz.variables(var_student).value
	local token = dz.variables(var_token).value
	local noSchoolToday = dz.variables(var_wakeUpdayOff).value
	local child1WakeUpSchool = Time(dz.variables(var_wakeUpCalculated).value).dDate


	-- Notion de temps
        local today = os.time{year=os.date("%Y"), month=os.date("%m"), day=os.date("%d"), hour=0, minute=0, sec=0}
        local tomorrow = today + 24*60*60
	local nextweek = today + 7*24*60*60

	if child1WakeUpSchool == nil then
	   child1WakeUpSchool = nextweek 
	end

	-- Dico
	local function dico(str)
	    if (dico_matiere[str] == nil) then 
                return str
	    else
	        return dico_matiere[str]
            end
        end

        -- Definition de functions d'écriture dans les logs
	local function logWrite(str,level)
            dz.log(tostring(str),level or dz.LOG_DEBUG)
        end
	logWrite(noSchoolToday.."<---------------------------------------------")
	-- Parse du Json pour affichange dans les logs
	local function logWriteJson(json)  
            if (type(json) == "table") then
                for i, result in pairs(json) do
                    if (type(json[i]) == "string") then
                        logWrite("ItemString["..i..']='..result, dz.LOG_DEBUG)
		    else
                        if (type(json[i]) == "table") then
                            logWrite("Table:"..tostring(i))
			    logWriteJson(json[i])
			else
                            logWrite("Item:"..tostring(i).."="..tostring(result))
		        end
                    end
                end
            end
        end
	-- Gestion des dates
	local function jsondate2day(input)
	    return os.date("%d/%m", input/1000)
	end
	local function jsondate2hour(input)
	    return os.date("%H:%M", input/1000)
	end
	local function jsondate2date(input)
	    return input/1000
	end
	local function date2day(input)
	    return os.date("%d/%m", input)
        end 
	local function date2hour(input)
	    return os.date("%H:%M", input)
	end
	local function date2string(input)
	    return os.date("%Y-%m-%d %H:%M:%S", input)
	end

	-- Procedure principale 	
	if item.isHTTPResponse then
	    logWrite(item.trigger)
            if (item.trigger == login_response) then
	        local json = dz.utils.fromJSON(item.data)
	       -- logWriteJson(json)
	       if json ~= nil then
	           token = json['token']  
	           dz.variables('pronoteToken').set(token)
	       else
	           token = nil
	           dz.variables('pronoteToken').set("")
	       end
	       if token ~= nil then
		    local query_from =  os.date("%m-%d-%Y",today) 
		    local query_to = os.date("%m-%d-%Y", tomorrow)
		   -- Requete d'info timetable
		   dz.openURL({
		                url = 'http://127.0.0.1:3333/graphql',
		                method = "POST",
		                postData = { ['query'] = '{ timetable(student:"'..student1..'") { subject, room, isCancelled, status, to, from } }', 
		                           },
		                headers = { ['Content-Type'] = 'application/json',
				            ['Token'] = token },
				callback = timetable_response})
	       end
	    elseif (item.trigger == timetable_response) then
		-- Reception de la timetable
	        local json = dz.utils.fromJSON(item.data)
	        -- logWriteJson(json)
		local text_timetable = ''
		local line_timetable = ''
		local premier_cours = tomorrow 
                local niveau = 0
		local loop = 1
		logWrite('-------------------------------------------------------------')
                for i, result in pairs(json['data']['timetable']) do
		    -- Preparation du texte de la timetable
		    local status = result['status']
		    local room = dico(result['room'])
		    local subject = dico(result['subject'])
                    -- Nombre de ligne a afficher
		    if loop == 5 then
                        break
		    end
		    loop = loop + 1
		    -- Si le cours est annulé
		    local absent = string.find(line_timetable, "bsent")
		    if (result['isCancelled']) or (absent ~= nil) or (status == "Cours annulé") then
			niveau = 3
		        line_timetable = jsondate2hour(result['from']).." | "..jsondate2hour(result['to']).." | "..subject.." | Cours Annulé\n"
		    else
		        -- Verification de l'heure du 1er cours non annulé
			local cours_suivant = jsondate2date(result['from'])
		        if (premier_cours > cours_suivant) and (cours_suivant < tomorrow) then
	                    premier_cours = cours_suivant 
		            logWrite(line_timetable.." Timetable ====> "..os.date("%d/%m/%Y %H:%M", premier_cours))
		        end

		        line_timetable = jsondate2hour(result['from']).." | "..jsondate2hour(result['to']).." | "..subject
                        if room ~= nil then
			    line_timetable = line_timetable.." | "..room
		        end
			-- Décommenter pour afficher le status
		        if status ~= nil then
		    	    line_timetable = line_timetable..' ('..status..')'
	                end

			line_timetable = line_timetable.."\n"
		    end
		    text_timetable = text_timetable..line_timetable
		end
		local sceneToActivate  = 'Scénario collège'
		local dev_resteMaison  = 'Automatismes'     -- Reste à la maison, ne pas activer la scene si allumé
		local reste = dz.devices(dev_resteMaison)
		-- Mise a jour de l'heure de reveil par défaut si nécessaire
		if  date2day(child1WakeUpSchool) ~= date2day(today) then
		    logWrite("Aucun reveil planifié aujourd'hui. Activation pour "..noSchoolToday )
                    child1WakeUpSchool = Time(os.date('%Y-%m-%d ', today)..noSchoolToday..':00').dDate
		    if (reste.levelName == "Tous" or reste.levelName == "Romain") then
		        dz.scenes(sceneToActivate).cancelQueuedCommands()
		        dz.scenes(sceneToActivate).switchOn().at(date2hour(child1WakeUpSchool))
		    end
		end
		-- Vérification de l'heure du premier cours pour mise a jour du reveil.
		logWrite("Heure du premier cours de la journée :"..date2string(premier_cours))
		local proposedWakeUpSchool = premier_cours-temp_de_reveil
		if (proposedWakeUpSchool < child1WakeUpSchool) then
		    logWrite("Mise à jour de l'heure de réveil à "..date2string(proposedWakeUpSchool))
		    if (reste.levelName == "Tous" or reste.levelName == "Romain") then
		        dz.scenes(sceneToActivate).cancelQueuedCommands()
		        dz.scenes(sceneToActivate).switchOn().at(date2hour(proposedWakeUpSchool))
		    end
		end
		if text_timetable == nil then
		    logWrite('Pas de cours à venir')
	        else
		    logWrite("Timetable a venir :\n"..text_timetable)
		end

		-- Mise a jour equipement Timetable
                local dTimetable = dz.devices(device_Student_Timetable)
                dTimetable.updateAlertSensor(niveau, text_timetable)
		
		-- Envoi de la requete d'info marks
 		dz.openURL({
		                url = 'http://127.0.0.1:3333/graphql',
		                method = "POST",
		                postData = { ['query'] = '{ marks(student:"'..student1..'", period:"year") { averages { student, studentClass } } }', 
		                           },
		                headers = { ['Content-Type'] = 'application/json',
				            ['Token'] = token },
				callback = marks_response})
	    elseif (item.trigger == marks_response) then
		-- Traitement de réponse de notes
	        local json = dz.utils.fromJSON(item.data)
	        --logWriteJson(json)
                local dStudentAverage = dz.devices(device_Student_Average)
                local dClassAverage = dz.devices(device_Class_Average)
		dStudentAverage.updateTemperature(json['data']['marks']['averages']['student'])
		dClassAverage.updateTemperature(json['data']['marks']['averages']['studentClass'])
		-- Envoi de la requete d'info homeworks jusqu'a demain 
		local query_from =  os.date("%m-%d-%Y",today) 
		local query_to = os.date("%m-%d-%Y", tomorrow)
 		dz.openURL({
		                url = 'http://127.0.0.1:3333/graphql',
		                method = "POST",
		                postData = { ['query'] = '{ homeworks(student:"'..student1..'", from:"'..query_from..'", to:"'..query_to..'") { description, subject, givenAt, for, done } }', 
		                           },
		                headers = { ['Content-Type'] = 'application/json',
				            ['Token'] = token },
				callback = homeworks_response})
	    elseif (item.trigger == homeworks_response) then
	        -- Traitement de la requete homework recue
	        local json = dz.utils.fromJSON(item.data)
	        -- logWriteJson(json)
		local nombre_total = 0
		local nombre_fait = 0
		local text_devoir_a_faire = ''
		local table_a_faire = {}
		-- logWrite('-------------------------------------------------------------')
                for i, result in pairs(json['data']['homeworks']) do
		    nombre_total = nombre_total + 1
		    if result['done'] ~= true then
			local subject = dico(result['subject'])
			--logWrite(tostring(i)..':'..subject..":"..result['description'])
			local date_for = jsondate2day(result['for'])
			--text_devoir_a_faire = text_devoir_a_faire..subject.." pour le "..date_for..'\n'
                        if (table_a_faire[subject] == nil) then
                            table_a_faire[subject] = 1
		        else
                            table_a_faire[subject] = table_a_faire[subject] + 1
		        end
		    else
	                nombre_fait = nombre_fait + 1
		    end
		end
	        logWriteJson(table_a_faire)
		for i,nb in pairs(table_a_faire) do
		    text_devoir_a_faire = text_devoir_a_faire.."   * "..i.." : "..tostring(nb).."\n"
		end
		--logWrite('-------------------------------------------------------------')
		local reste = nombre_total - nombre_fait
		local entete_devoir_a_faire = 'Total : '..tostring(nombre_total).."  Fait : "..tostring(nombre_fait).."  Reste : "..tostring(reste)
		--logWrite(entete_devoir_a_faire)
		--logWrite("\n"..text_devoir_a_faire)
		--logWrite('-------------------------------------------------------------')
		text_devoir_a_faire = entete_devoir_a_faire.."\n"..text_devoir_a_faire
	        -- Mise a jour du nombre de devoir a faire 
		local dHomeworksTodoTom = dz.devices(device_Student_Todo_Tom)
		dHomeworksTodoTom.updateCustomSensor(reste)
		-- Mise a jour de l'equipement listant les devoirs 
                local dTimetable = dz.devices(device_Student_Todo_Resume)
		if reste > 5 then
		    reste = 5
	        end
                dTimetable.updateAlertSensor(reste, text_devoir_a_faire)
                -- Delog
	        dz.openURL({
	                        url = 'http://127.0.0.1:3333/auth/logout',
	                        method = "POST",
		                headers = { ['Content-Type'] = 'application/json',
		                            ['Token'] = token },
				postData = {},
		                callback = logout_response})
	    elseif (item.trigger == logout_response) then
	        local json = dz.utils.fromJSON(item.data)
	        logWrite("Reponse de logout")
	        -- logWriteJson(json)
	        dz.variables('pronoteToken').set("")
	    else
	        logWrite("Reponse inconnue recue")
	    end
        else
	    -- Login
	    token = nil
            dz.openURL({
                  url = 'http://127.0.0.1:3333/auth/login',
                        method = "POST",
			postData = { ['url'] = 'https://0601766u.index-education.net/pronote',
			             ['username'] = pronoteLogin,
			             ['password'] = pronotePassword,
				     ['cas'] = 'hdf',
				     ['account'] = 'parent'
				   },
		        headers = { ['Content-Type'] = 'application/json' },
                        callback = login_response})
        end
    end
}
