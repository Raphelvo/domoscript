--[[
alerteMeteoFrancePluie.lua
author/auteur = papoo
update/mise à jour = 11/05/2019
création = 09/03/2019
https://pon.fr/dzvents-alerte-previsions-de-pluie-prochaines-60-minutes
https://github.com/papo-o/domoticz_scripts/blob/master/dzVents/scripts/alerteMeteoFrancePluie.lua
https://easydomoticz.com/forum/viewtopic.php?f=17&t=5492

Principe : récupérer via l'API non documentée de météo France 
les informations de précipitation de votre commune sur un device alert et/ou text
la carte des départements couverts par ce service (ils ne le sont pas tous)
http://www.meteofrance.com/previsions-meteo-france/previsions-pluie

--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local alert_device  = "Pluie dans l'heure"
local text_alert    = "Info Du Jour"
local alarm_button  = "Alerte pluie"

local echo_name     = 'EchoSalon_Speak'
local echo_url      = 'http://192.168.1.20:8080/basicui/CMD'
--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'météo France alerte pluie'
local scriptVersion     = '1.0'
local response          = "meteoFrance_pluie"
local alexa_response    = "alexa_speak"
return {
    active = true,
    on =        {       timer           =   { "every 5 minutes" },
                        httpResponses   =   {  response } },

    logging =   {   --level    =   domoticz.LOG_DEBUG,
                    --level    =   domoticz.LOG_INFO,      -- Seulement un niveau peut être actif; commenter les autres
                    level    =   domoticz.LOG_ERROR,     -- Only one level can be active; comment others
                    -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                    marker  =   scriptName..' v'..scriptVersion },

    execute = function(dz, item)
        local Time = require('Time')
        local devAlert       = dz.devices(alert_device)
        local devAlarmButton = dz.devices(alarm_button)

        local function logWrite(str,level)
            dz.log(tostring(str),level or dz.LOG_DEBUG)
        end
        local function seuilAlerte(level)
            if level == 0 or level == nil then return dz.ALERTLEVEL_GREY end
            if level == 1 then return dz.ALERTLEVEL_GREEN end
            if level == 2 then return dz.ALERTLEVEL_YELLOW end
            if level == 3 then return dz.ALERTLEVEL_ORANGE end
            if level == 4 then return dz.ALERTLEVEL_RED end
        end
        local function raiseAlarm(devButton, text)
            local heureCourante = os.date('%H:%M')
	    local heureCourantePlus30mn = os.date("%H:%M",  os.time(Time(dz.time.raw)) + 30 * 60 ) 
	    local heureDerniereAlarmePluie = dz.variables('alarmLatestRain').value
	    local heureMin = dz.variables('alarmStart').value
	    local heureMax = dz.variables('alarmEnd').value
	    local heureMinVacances = dz.variables('alarmStartHoliday').value
	    local heureMaxVacances = dz.variables('alarmEndHoliday').value
	    local heureMinChoisie, heureMaxChoisie
	    -- On se leve tard le WE, les jours fériés et les vacances
	    local vacancesAujourdhui = dz.variables('dayOff').value 
	    -- On ne se couche tard que pendant les vacances
	    local vacancesDemain = dz.variables('dayHolidayTomorrow').value
	    local alarmAutorise = false
	    local repetition = false

	    -- Recherche des heures possible d'alarme
	    if vacancesAujourdhui == "true" then -- C est une chaine, pas un bool
                heureMinChoisie = heureMinVacances
	    else
                heureMinChoisie = heureMin
	    end
	    if vacancesDemain == "true" then 
                heureMaxChoisie = heureMaxVacances
	    else
                heureMaxChoisie = heureMax
	    end
	    -- Inderdiction d'une alarme a chaque appels de cette fonction
 	    if heureCourante >= heureDerniereAlarmePluie then
	        repetition = true
		logWrite("Autorisation de repeter l'alarme pluie vocale")
	    end

            -- On verifie si l'alarme est autorise 
            if ((heureCourante > heureMinChoisie) and (heureCourante < heureMaxChoisie) and (repetition)) then
                alarmAutorise = true
		-- logWrite("Autorisation d'activer l'alarme pluie vocale")
	    end
            logWrite("raiseAlarm("..devButton.state.."-"..tostring(alarmAutorise)..") de "..heureMinChoisie.." à "..heureMaxChoisie..": "..text.." Bloqué:"..heureDerniereAlarmePluie, dz.LOG_INFO)
            if (devButton.state == 'On' ) and (alarmAutorise == true) then
                dz.variables('alarmLatestRain').set(heureCourantePlus30mn)
                dz.variables('alarmLatestRainText').set(text)
                local textALireURL = dz.utils.urlEncode(text,"%20")
		dz.openURL(echo_url.."?"..echo_name.."="..textALireURL)
		dz.openURL({ url = echo_url.."?"..echo_name.."="..textALireURL,
		             method = "GET",
		             callback = alexa_response})
            end
	end
	local function date2string(entier)
	    return os.date("%c", entier)
	end

	local function date2stringheure(entier)	
	    return tonumber(os.date("%H", entier))..":"..(os.date("%M", entier))..""
        end 

        if (item.isHTTPResponse and item.ok) then
	    if (item.trigger == response) then
	        local json = dz.utils.fromJSON(item.data)
	        local forecast = json["forecast"]
	        local position = json["position"]
	        local mise_a_jour = json["updated_on"]
	        local pluie_max = 0 
	        local pluie_max_texte = "Temps sec"
	        local pluie_debut = nil
            
	        logWrite("Date prévision : "..date2stringheure(mise_a_jour))
	        for i = 1, 9, 1  -- Boucle dans les 9 niveaux de prévisions
                do
	            local rain = forecast[i]["rain"]
		    local desc = forecast[i]["desc"]
		    local dt = forecast[i]["dt"]
		    local h = date2stringheure(dt)
	            logWrite("==> "..rain..":"..desc.." à "..h)
		    if (pluie_debut == nil) and (rain > 1) then -- 1ere detection de pluie
		        pluie_debut = dt
		    end
		    if (rain > pluie_max) then -- a chaque detection de pluie plus forte
		        pluie_max = rain
		        pluie_max_texte = desc
		    end
                end
	        if (pluie_max > 1) then -- Si pluie prévue dans l'heure
	            local mn = tonumber(os.date("%M", (os.difftime(pluie_debut, os.time()))))
		    local alert_text = "Alerte ! "..pluie_max_texte.." prévue dans "..mn.." minutes"
		    devAlert.updateAlertSensor(seuilAlerte(pluie_max), alert_text)
		    raiseAlarm(devAlarmButton, alert_text)
	        else
		    devAlert.updateAlertSensor(seuilAlerte(1), "Pas de pluie dans l'heure")
		    -- raiseAlarm(devAlarmButton, "Pas de pluie dans l'heure")
	        end
            end
        else
            local url = 'http://webservice.meteofrance.com/rain?token=__Wj7dVSTjV9YGu1guveLyDq0g7S7TfTjaHBTPTpO0kj8__&lat=49.12&lon=2.52&lang=fr&id='
            dz.openURL({
                  url = url,
                        method = "GET",
                        callback = response})
        end
    end
}
