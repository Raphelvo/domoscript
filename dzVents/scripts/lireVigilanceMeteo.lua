--[[ alertePollens.lua for [ domoticzVents >= 2.4 ]

author/auteur = papoo
update/mise à jour = 05/05/2019
creation = 03/04/2019
https://pon.fr/dzvents-alerte-pollens
https://github.com/papo-o/domoticz_scripts/blob/master/dzVents/scripts/alertePollens.lua
https://easydomoticz.com/forum/viewtopic.php?f=17&t=8392
--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local button_device            = 'Lire vigilance météo' -- nom ou idx du device alerte, nil si inutilisé

local alert_device             = 'Vigilance Météo'    -- renseigner le nom de l'éventuel device alert vigilance météo associé (dummy - alert)
local conseil_meteo            = "Conseil Météo" --'Conseil Météo'      -- renseigner le nom de l'éventuel device alert Conseils Météo associé si souhaité, sinon nil 
local commentaire_meteo        = "Commentaire Météo" --'Commentaire Météo'  -- renseigner le nom de l'éventuel device alert Commentaire Météo associé si souhaité, sinon nil
local echo_name                = 'EchoSalon_Speak'
local echo_url                 = 'http://192.168.1.20:8080/basicui/CMD'
--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'Lire météo'
local scriptVersion     = '1.12'


return {
           active = true,
           on     =   {   devices  =   { button_device },
	                  timer    =   { 'at 12:55' }
                      },

           logging =  {
                       --   level    =   domoticz.LOG_DEBUG,                                           -- Seulement un niveau peut être actif; commenter les autres
                       -- level    =   domoticz.LOG_INFO,                                            -- Only one level can be active; comment others
                       level    =   domoticz.LOG_ERROR,
                       -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                       marker = scriptName..' v'..scriptVersion 
	              },
    
           execute = function(domoticz, item)

	       local textAlert = tostring(domoticz.devices(alert_device).text)
	       local textConseil = tostring(domoticz.devices(conseil_meteo).text)
	       local textCommentaire = tostring(domoticz.devices(commentaire_meteo).text)
	       domoticz.log("A:1:"..textAlert..":2:"..textConseil..":3:"..textCommentaire..":", domoticz.LOG_DEBUG)
	       local ALire = true 
	       local textALire = ''
	       if (item.isTimer) then
	           if textAlert == "Aucune vigilance" then
	               ALire = false
		   end
	       end
	       textALire = "Alerte météo. "..textAlert..". "
	       domoticz.log("B:"..textALire, domoticz.LOG_DEBUG)
	       if textConseil ~= "Aucun conseil disponible" then
	           textALire = textALire..textConseil
	       end
	       domoticz.log("C:"..textALire, domoticz.LOG_DEBUG)
	       if textCommentaire ~= "Aucun commentaire disponible" then
                   textALire = textALire..textCommentaire
	       end
	       domoticz.log("D:"..textALire..':D', domoticz.LOG_DEBUG)
	       local textALireURL = domoticz.utils.urlEncode(textALire,"%20")
	       if (ALire and (textALire ~= '' or textALire ~= nil) ) then      
	           domoticz.openURL(echo_url.."?"..echo_name.."="..textALireURL)
	       end
          end
}
