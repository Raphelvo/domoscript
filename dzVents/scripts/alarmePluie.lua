--[[ modePhilou.lua for [ domoticzVents >= 2.4 ]

author/auteur = raphelvo
update/mise à jour = 20/10/2020
creation = 20/10/2020
--]]
--------------------------------------------
------------ Variables à éditer ------------
--------------------------------------------
local deviceActivate = 'Alerte pluie' -- nom ou idx du device de declenchement 

local echo_name     = 'EchoSalon_Speak'
local echo_url      = 'http://192.168.1.20:8080/basicui/CMD'
--------------------------------------------
----------- Fin variables à éditer ---------
--------------------------------------------
local scriptName        = 'Boutton alarme pluie'
local scriptVersion     = '1.0'

return { 
    active  = true,
    on      =   {   
                    devices  =  { deviceActivate }
                },

    logging =   {
                   level    =   domoticz.LOG_DEBUG,       -- Seulement un niveau peut être actif; commenter les autres
                   -- level    =   domoticz.LOG_INFO,          
                   -- level    =   domoticz.LOG_ERROR,
                   -- level    =   domoticz.LOG_MODULE_EXEC_INFO,
                   marker =  scriptName..' v'..scriptVersion },
	        
    execute = function(domoticz, item)
    
        local function logWrite(str,level)         -- Support function for shorthand debug log statements
            domoticz.log(tostring(str),level or domoticz.LOG_DEBUG)
        end
        
        local dButton = domoticz.devices(deviceActivate)
	local vocalText

	if (dButton.state == 'On' ) then
            vocalText = "Activation de l'alerte pluie"
	    domoticz.variables('alarmLatestRain').set("00:00")
	    domoticz.variables('alarmLatestRainText').set("Activation par interrupteur")
	else
	    vocalText = "Désactivation de l'alerte pluie"
	end
	local textALireURL = domoticz.utils.urlEncode(vocalText,"%20")
	logWrite(vocalText)
        domoticz.openURL(echo_url.."?"..echo_name.."="..textALireURL )
    end
} 
